package main

import (
	"gitlab.com/jbgo/terraform-provider-es/es"
)

func flattenSchedule(schedule *es.WatchSchedule) []map[string]interface{} {
	if len(schedule.Interval) > 0 {
		return []map[string]interface{}{
			{"interval": schedule.Interval},
		}
	} else {
		return []map[string]interface{}{
			{"daily": map[string]string{"at": schedule.Daily.At[0]}},
		}
	}
}

func expandSchedule(schedule []interface{}) *es.WatchSchedule {
	elem := schedule[0].(map[string]interface{})

	value, ok := elem["interval"]
	if ok {
		interval := value.(string)
		if len(interval) > 0 {
			return &es.WatchSchedule{Interval: interval}
		}
	}

	value, ok = elem["daily"]
	if ok {
		daily := value.(map[string]interface{})
		return &es.WatchSchedule{
			Daily: &es.WatchScheduleDaily{
				At: []string{daily["at"].(string)},
			},
		}
	}

	return nil
}
