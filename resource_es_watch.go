package main

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"gitlab.com/jbgo/terraform-provider-es/es"
)

func resourceWatch() *schema.Resource {

	return &schema.Resource{
		Create: resourceWatchCreate,
		Read:   resourceWatchRead,
		Update: resourceWatchUpdate,
		Delete: resourceWatchDelete,
		Schema: watcherWatchSchema(),
	}
}

func watcherWatchSchema() map[string]*schema.Schema {
	return map[string]*schema.Schema{
		"watch_id": {
			Type:     schema.TypeString,
			Required: true,
		},

		"active": {
			Type:     schema.TypeBool,
			Optional: true,
			Default:  true,
		},

		"schedule": &schema.Schema{
			Type:     schema.TypeList,
			Required: true,
			MaxItems: 1,
			Elem: &schema.Resource{
				Schema: map[string]*schema.Schema{
					"daily": {
						Type:          schema.TypeMap,
						Optional:      true,
						ConflictsWith: []string{"schedule.0.interval"},
					},
					"interval": {
						Type:          schema.TypeString,
						Optional:      true,
						ConflictsWith: []string{"schedule.0.daily"},
					},
				},
			},
		},

		// "throttle_period": {
		// 	Type:          schema.TypeInt,
		// 	Optional:      true,
		// 	Default:       5,
		// 	ConflictsWith: []string{"throttle_period_in_millis"},
		// },

		// "throttle_period_in_millis": {
		// 	Type:          schema.TypeInt,
		// 	Optional:      true,
		// 	ConflictsWith: []string{"throttle_period"},
		// },
	}
}

func resourceWatchCreate(d *schema.ResourceData, m interface{}) error {
	watchId := d.Get("watch_id").(string)
	active := d.Get("active").(bool)
	reqData := &es.PutWatchRequest{
		Trigger: &es.WatchTrigger{
			Schedule: expandSchedule(d.Get("schedule").([]interface{})),
		},
	}

	putWatchResp, err := esClient.PutWatch(watchId, active, reqData)
	if err != nil {
		d.SetId("")
		return err
	}

	d.SetId(putWatchResp.Id)
	return resourceWatchRead(d, m)
}

func resourceWatchRead(d *schema.ResourceData, m interface{}) error {
	watchId := d.Get("watch_id").(string)
	getWatchResp, err := esClient.GetWatch(watchId)
	if err != nil {
		return err
	}

	if !getWatchResp.Found {
		d.SetId("")
		return nil
	}

	d.SetId(getWatchResp.Id)
	d.Set("watch_id", getWatchResp.Id)
	d.Set("active", getWatchResp.Status.State.Active)
	d.Set("schedule", flattenSchedule(getWatchResp.Watch.Trigger.Schedule))

	return nil
}

func resourceWatchUpdate(d *schema.ResourceData, m interface{}) error {
	// For now, Create/Update are the same. Will see how long that lasts...
	return resourceWatchCreate(d, m)
}

func resourceWatchDelete(d *schema.ResourceData, m interface{}) error {
	watchId := d.Get("watch_id").(string)
	err := esClient.DeleteWatch(watchId)
	if err == nil {
		d.SetId("")
	}

	return err
}
