package main

import (
	"gitlab.com/jbgo/terraform-provider-es/es"
	"reflect"
	"testing"
)

func TestFlattenScheduleInterval(t *testing.T) {
	input := &es.WatchSchedule{Interval: "6s"}
	expected := []map[string]interface{}{{"interval": "6s"}}
	actual := flattenSchedule(input)
	assertDeepEqual(t, expected, actual)
}

func TestFlattenScheduleDaily(t *testing.T) {
	input := &es.WatchSchedule{
		Daily: &es.WatchScheduleDaily{
			At: []string{"3:33"},
		},
	}
	expected := []map[string]interface{}{
		{
			"daily": map[string]string{
				"at": "3:33",
			},
		},
	}
	actual := flattenSchedule(input)
	assertDeepEqual(t, expected, actual)
}

func TestESExpandScheduleInterval(t *testing.T) {
	input := []interface{}{map[string]interface{}{"interval": "6s"}}
	expected := &es.WatchSchedule{Interval: "6s"}
	actual := expandSchedule(input)
	assertDeepEqual(t, expected, actual)
}

func TestESExpandScheduleDaily(t *testing.T) {
	input := []interface{}{
		map[string]interface{}{
			"daily": map[string]interface{}{
				"at": "3:33",
			},
			"interval": "",
		},
	}
	expected := &es.WatchSchedule{
		Daily: &es.WatchScheduleDaily{
			At: []string{"3:33"},
		},
	}
	actual := expandSchedule(input)
	assertDeepEqual(t, expected, actual)
}

func assertDeepEqual(t *testing.T, expected interface{}, actual interface{}) {
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf(
			"Expansion failed.\n\tExpected: %#v\n\tActual: %#v",
			expected,
			actual)
	}
}
