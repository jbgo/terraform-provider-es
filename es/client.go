package es

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type Client struct {
	BaseUrl string
	*http.Client
}

const DefaultUrl = "http://localhost:9200"

func MakeClient() *Client {
	client := Client{DefaultUrl, &http.Client{}}
	if url, ok := os.LookupEnv("ELASTICSEARCH_URL"); ok {
		client.BaseUrl = url
	}
	return &client
}

func (client *Client) GetWatch(watchId string) (*GetWatchResponse, error) {
	url := fmt.Sprintf("%s/_watcher/watch/%s", client.BaseUrl, watchId)

	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var getWatchResp GetWatchResponse
	err = json.Unmarshal(buf, &getWatchResp)
	return &getWatchResp, err
}

func (client *Client) PutWatch(watchId string, active bool, reqData *PutWatchRequest) (*PutWatchResponse, error) {
	url := fmt.Sprintf("%s/_watcher/watch/%s?active=%t", client.BaseUrl, watchId, active)

	reqBuf, err := json.Marshal(reqData)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("PUT", url, bytes.NewReader(reqBuf))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if !(resp.StatusCode == 200 || resp.StatusCode == 201) {
		var errResp ErrorResponse
		err = json.Unmarshal(buf, &errResp)
		if err != nil {
			return nil, fmt.Errorf(
				"PutWatch API Error [watch_id=%s] %s: %w)",
				watchId, resp.Status, err)
		}
		return nil, fmt.Errorf("PutWatch API Error [watch_id=%s] %s: [%s] %s",
			watchId, resp.Status, errResp.Error.Type, errResp.Error.Reason)
	}

	var putWatchResp PutWatchResponse
	err = json.Unmarshal(buf, &putWatchResp)
	return &putWatchResp, err
}

func (client *Client) DeleteWatch(watchId string) error {
	url := fmt.Sprintf("%s/_watcher/watch/%s", client.BaseUrl, watchId)

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	_, err = client.Do(req)
	return err
}
