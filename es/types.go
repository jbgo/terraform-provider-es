package es

type GetWatchResponse struct {
	Found  bool
	Id     string `json:"_id"`
	Status *WatchStatus
	Watch  *Watch
}

type PutWatchRequest struct {
	Trigger                *WatchTrigger   `json:"trigger"`
	Input                  *WatchInput     `json:"input,omitempty"`
	Condition              *WatchCondition `json:"condition,omitempty"`
	Actions                *WatchActions   `json:"actions,omitempty"`
	Metadata               string          `json:"metadata,omitempty"`
	ThrottlePeriod         int64           `json:"throttle_period,omitempty"`
	ThrottlePeriodInMillis int64           `json:"throttle_period_in_millis,omitempty"`
}

type PutWatchResponse struct {
	Id      string `json:"_id"`
	Created bool
}

type ErrorResponse struct {
	Error *Error
}

type Error struct {
	Type      string
	Reason    string
	CausedBy  *Error
	RootCause []*Error
}

type Watch struct {
	Trigger   *WatchTrigger `json:"trigger"`
	Input     *WatchInput
	Condition *WatchCondition
	Actions   *WatchActions
}

type WatchStatus struct {
	State   *WatchState
	Actions *WatchActions
}

type WatchState struct {
	Active    bool
	Timestamp string
}

type WatchActions struct{}

type WatchTrigger struct {
	Schedule *WatchSchedule `json:"schedule"`
}

type WatchSchedule struct {
	Cron     string              `json:"cron,omitempty"`
	Interval string              `json:"interval,omitempty"`
	Hourly   interface{}         `json:"hourly,omitempty"`
	Daily    *WatchScheduleDaily `json:"daily,omitempty"`
	Weekly   interface{}         `json:"weekly,omitempty"`
	Monthly  interface{}         `json:"monthly,omitempty"`
	Yearly   interface{}         `json:"yearly,omitempty"`
}

type WatchScheduleDaily struct {
	At []string `json:"at"`
}

type WatchInput struct {
	None interface{}
}

type WatchCondition struct {
	Always interface{}
}
