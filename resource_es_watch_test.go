package main

import (
	"fmt"
	"github.com/hashicorp/terraform-plugin-sdk/helper/acctest"
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
	"gitlab.com/jbgo/terraform-provider-es/es"
	"regexp"
	"testing"
)

func TestAccESWatch_minimal(t *testing.T) {
	esClient = es.MakeClient()

	var watchResp es.GetWatchResponse
	resourceName := acctest.RandStringFromCharSet(10, acctest.CharSetAlphaNum)

	resource.Test(t, resource.TestCase{
		Providers: testAccProviders,
		Steps: []resource.TestStep{
			{
				Config: testAccESWatchResource(resourceName),
				Check: resource.ComposeTestCheckFunc(
					testAccESWatchExists("es_watch.minimal", &watchResp),
					resource.TestCheckResourceAttr("es_watch.minimal", "active", "true"),
					resource.TestCheckResourceAttr("es_watch.minimal", "schedule.0.interval", "5m"),
				),
			},
			{
				Config: testAccESWatchResourceUpdate(resourceName),
				Check: resource.ComposeTestCheckFunc(
					testAccESWatchExists("es_watch.minimal", &watchResp),
					resource.TestCheckResourceAttr("es_watch.minimal", "active", "false"),
					resource.TestCheckResourceAttr("es_watch.minimal", "schedule.0.interval", "30s"),
				),
			},
			{
				Config:      testAccESWatchResourceScheduleConflict(resourceName),
				ExpectError: regexp.MustCompile("conflicts with schedule"),
			},
			{
				Config:      testAccESWatchResourceScheduleInvalidInterval(resourceName),
				ExpectError: regexp.MustCompile("unrecognized interval format"),
			},
			{
				Config: testAccESWatchResourceUpdateDaily(resourceName),
				Check: resource.ComposeTestCheckFunc(
					testAccESWatchExists("es_watch.minimal", &watchResp),
					resource.TestCheckResourceAttr("es_watch.minimal", "active", "true"),
					resource.TestCheckResourceAttr("es_watch.minimal", "schedule.0.daily.at", "5:55"),
					resource.TestCheckResourceAttr("es_watch.minimal", "schedule.0.interval", ""),
				),
			},
		},
		CheckDestroy: testAccESWatchMissing("es_watch.minimal"),
	})
}

func testAccESWatchResource(name string) string {
	return fmt.Sprintf(`
resource "es_watch" "minimal" {
	watch_id = "%s"
	schedule {
		interval = "5m"
	}
}
`, name)
}

func testAccESWatchResourceUpdate(name string) string {
	return fmt.Sprintf(`
resource "es_watch" "minimal" {
	watch_id = "%s"
	active = false
	schedule {
		interval = "30s"
	}
}
`, name)
}

func testAccESWatchResourceUpdateDaily(name string) string {
	return fmt.Sprintf(`
resource "es_watch" "minimal" {
	watch_id = "%s"
	active = true
	schedule {
    daily = { "at" = "5:55" }
	}
}
`, name)
}

func testAccESWatchResourceScheduleConflict(name string) string {
	return fmt.Sprintf(`
resource "es_watch" "minimal" {
	watch_id = "%s"
	schedule {
    interval = "5m"
    daily = { "at" = "5:55" }
	}
}
`, name)
}

func testAccESWatchResourceScheduleInvalidInterval(name string) string {
	return fmt.Sprintf(`
resource "es_watch" "minimal" {
	watch_id = "%s"
	schedule {
    interval = "never"
	}
}
`, name)
}

func testAccESWatchExists(resourceName string, watchResp *es.GetWatchResponse) resource.TestCheckFunc {
	return func(s *terraform.State) (err error) {
		tfResource, ok := s.RootModule().Resources[resourceName]
		if !ok {
			return fmt.Errorf("Resource not found in state: %s", resourceName)
		}

		watchResp, err = esClient.GetWatch(tfResource.Primary.ID)
		if err != nil {
			return fmt.Errorf("Resource not found: %s: es.Watch{watch_id: %s} %w", resourceName, tfResource.Primary.ID, err)
		}

		return nil
	}
}

func testAccESWatchMissing(resourceName string) resource.TestCheckFunc {
	return func(s *terraform.State) (err error) {
		tfResource, ok := s.RootModule().Resources[resourceName]
		if !ok {
			return fmt.Errorf("Resource not found in state: %s", resourceName)
		}

		watchResp, err := esClient.GetWatch(tfResource.Primary.ID)
		if err == nil && watchResp.Found {
			fmt.Errorf("Resource found: %s: es.Watch{watch_id: %s}, %#v", resourceName, tfResource.Primary.ID, watchResp)
		}

		return err
	}
}
