# terraform-provider-es

An unofficial Terraform provider for the Elastic Stack.

**Status: Very much a work in progress (WIP)**

## Development

You will want to have a test Elasticsearch cluster to do any meaningful development.

    docker run -d --name elastic -p 9200:9200 -p 9300:9300 \
        -e "discovery.type=single-node" \
        docker.elastic.co/elasticsearch/elasticsearch:7.4.2

Some provider resources [require a commercial Elastic Stack subscription](https://www.elastic.co/subscriptions). For development, you can [enable a 30-day free trial](https://www.elastic.co/guide/en/elasticsearch/reference/current/start-trial.html).

     curl -XPOST 'http://localhost:9200/_license/start_trial?acknowledge=true'
     {
       "acknowledged":true,
       "trial_was_started":true,
       "type":"trial"
     }

Build the plugin.

    go build -o terraform-provider-es

Test the plugin.

    terraform init
    terraform plan
