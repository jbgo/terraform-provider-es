default: test
.PHONY: test testacc

test:
	go test -v

testacc:
	TF_ACC=1 go test -v
